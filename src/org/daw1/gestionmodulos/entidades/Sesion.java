/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestionmodulos.entidades;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
/**
 *
 * @author rgcenteno
 */
public class Sesion implements Comparable<Sesion>{
    private LocalTime horaEntrada;
    private LocalTime horaSalida;
    private java.time.DayOfWeek diaClase;
    

    public Sesion(LocalTime horaEntrada, LocalTime horaSalida, DayOfWeek diaClase) {
        checkDiaClase(diaClase);
        checkEntradaSalida(horaEntrada, horaSalida);
        this.horaEntrada = horaEntrada;
        this.horaSalida = horaSalida;
        this.diaClase = diaClase;        
    }
    
    private static void checkDiaClase(DayOfWeek dia){
        if(dia == null){
            throw new IllegalArgumentException("No se permiten valor null en el día");
        }
        else if(dia.equals(DayOfWeek.SATURDAY) || dia.equals(DayOfWeek.SUNDAY)){
            throw new IllegalArgumentException("No se permiten sesiones en fin de semana");
        }
    }
    
    private static void checkEntradaSalida(LocalTime horaEntrada, LocalTime horaSalida){
        if(horaEntrada == null || horaSalida == null){
            throw new IllegalArgumentException("No se permiten valores null en las horas de entrada y de salida");
        }
        if(horaEntrada.isAfter(horaSalida) || horaEntrada.equals(horaSalida)){
            throw new IllegalArgumentException("La hora de entrada debe de ser anterior a la hora de salida");
        }
        //horaSalida.minusHours(horaEntrada.get(java.time.temporal.ChronoField.HOUR_OF_DAY)).minusMinutes(horaEntrada.get(java.time.temporal.ChronoField.MINUTE_OF_HOUR));
        LocalTime auxiliar = horaSalida.minusMinutes(horaEntrada.get(java.time.temporal.ChronoField.MINUTE_OF_DAY));
        long minutos = auxiliar.get(java.time.temporal.ChronoField.MINUTE_OF_DAY);
        if(minutos % Modulo.getDuracionSesionModulo() != 0){
            throw new IllegalArgumentException("La duración debe ser múltiplo de " + Modulo.getDuracionSesionModulo() + " duración de " + minutos + " detectada ");
        }
    }
    
    /**
     * Devuelve a cuantas sesiones de clase equivale el horario de este objeto
     * @return número de sesiones de clase
     */
    public int getCuantasSesiones(){
        int sesiones = (this.horaSalida.get(ChronoField.MINUTE_OF_DAY) - this.horaEntrada.get(ChronoField.MINUTE_OF_DAY)) / Modulo.getDuracionSesionModulo();
        return sesiones;
    }

    @Override
    public int compareTo(Sesion t) {
        if(t == null){
            throw new NullPointerException();
        }
        else{
            if(this.diaClase.compareTo(t.diaClase) != 0){
                return this.diaClase.compareTo(t.diaClase);
            }
            else if(this.horaEntrada.compareTo(t.horaEntrada) != 0){
                return this.horaEntrada.compareTo(t.horaEntrada);
            }
            else{
                return this.horaSalida.compareTo(t.horaSalida);
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        else if(!(obj instanceof Sesion)){
            return false;
        }
        else{
            Sesion aux = (Sesion)obj;
            return this.diaClase.equals(aux.diaClase) && this.horaEntrada.equals(aux.horaEntrada) && this.horaSalida.equals(aux.horaSalida);
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }    

    @Override
    public String toString() {
        return this.diaClase + ". " + this.horaEntrada + " - " + this.horaSalida;
    }
    
}
