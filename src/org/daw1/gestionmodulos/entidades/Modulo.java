/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestionmodulos.entidades;

import java.util.SortedSet;

/**
 *
 * @author rgcenteno
 */
public class Modulo implements Comparable<Modulo>{
    private final String nombreModulo;
    private final String codigoModulo;
    private int sesionesSemanales;
    private String profesorTitular;
    private final SortedSet<Sesion> sesiones;
    
    private static int duracionSesionModulo = 50;
    
    public Modulo(String codigoModulo, String nombreModulo, String profesorTitular){
        checkCodigoModulo(codigoModulo);
        checkIsNotBlank(nombreModulo);
        checkIsNotBlank(profesorTitular);        
        this.codigoModulo = codigoModulo;
        this.nombreModulo = nombreModulo;
        this.profesorTitular = profesorTitular;
        this.sesionesSemanales = 0;
        this.sesiones = new java.util.TreeSet<>();
    }
    
    private static void checkCodigoModulo(String codigoModulo){
        if(codigoModulo == null || codigoModulo.isBlank() || codigoModulo.length() > 4){
            throw new IllegalArgumentException("Código de módulo debe tener una longitud de entre 1 y 4 caracteres imprimibles");
        }
    }
    
    private static void checkIsNotBlank(String text){
        if(text == null || text.isBlank()){
            throw new IllegalArgumentException("No se permiten valores nulos o cadenas en blanco");
        }
    }
    
    private static void checkSesionesSemanales(int sesionesSemanales){
        if(sesionesSemanales < 1 || sesionesSemanales > 30){
            throw new IllegalArgumentException("Valores permitidos entre 1 y 30");
        }
    }
    
    public String getNombreModulo(){
        return this.nombreModulo;
    }

    public String getCodigoModulo() {
        return codigoModulo;
    }

    public String getProfesorTitular() {
        return profesorTitular;
    }

    public int getSesionesSemanales() {
        return sesionesSemanales;
    }

    public static int getDuracionSesionModulo() {
        return duracionSesionModulo;
    }    

    public void setProfesorTitular(String profesor){
        checkIsNotBlank(profesor);
        this.profesorTitular = profesor;
    }

    @Override
    public String toString() {
        return this.codigoModulo + " - " + this.nombreModulo; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        else if(this == obj){
            return true;
        }
        else if(!(obj instanceof Modulo)){
            return false;
        }
        else{
            Modulo otro = (Modulo)obj;            
            return this.codigoModulo.equals(otro.getCodigoModulo()) && this.nombreModulo.equals(otro.getNombreModulo());
        }
    }

    @Override
    public int compareTo(Modulo t) {
        return (this.codigoModulo + this.nombreModulo).compareTo(t.codigoModulo + t.nombreModulo);
    }   

    @Override
    public int hashCode() {
        return (this.codigoModulo + this.nombreModulo).hashCode();
    }
    
    /**
     * Elimina una sesión al módulo actualizando el número de sesiones semanales
     * @param s Sesión a agregar
     * @return true si se agrega la sesión. False otherwise
     */
    public boolean agregarSesion(Sesion s){
        if(this.sesiones.add(s)){
            this.sesionesSemanales += s.getCuantasSesiones();
            return true;
        }
        else{
            return false;
        }
        
    }
    
    /**
     * Elimina una sesión al módulo actualizando el número de sesiones semanales
     * @param s Sesión a eliminar
     * @return true si se encuentra y elimina la sesión. False otherwise
     */
    public boolean quitarSesion(Sesion s){
        if(this.sesiones.remove(s)){
            this.sesionesSemanales -= s.getCuantasSesiones();
            return true;
        }
        else{
            return false;
        }
    }
    
    public boolean quitarSesion(int posicion){
        if(posicion <= 1 || posicion > this.sesiones.size()){
            return false;
        }
        else{
            Sesion actual = null;
            java.util.Iterator<Sesion> it = this.sesiones.iterator();
            for(int i = 0; i < posicion; i++){
                actual = it.next();
            }
            //Aquí ya tenemos en actual el objeto que queremos borrar
            return quitarSesion(actual);
        }
    }
    
    /**
     * Devuelve una copia no editable del conjunto donde se almacenan las sesiones
     * @return copia no editable del conjunto donde se almacenan las sesiones
     */
    public SortedSet<Sesion> getSesiones(){
        return java.util.Collections.unmodifiableSortedSet(this.sesiones);
    }

    public String getSesionesToString(){
        return this.sesiones.toString();
    }
    
    public void borrarHorario(){
        this.sesiones.clear();
    }
    
    public static void setDuracionSesionModulo(int duracion){
        if(duracion <= 0){
            throw new IllegalArgumentException("La duración debe ser mayor que cero.");
        }
        Modulo.duracionSesionModulo = duracion;
    }
}
