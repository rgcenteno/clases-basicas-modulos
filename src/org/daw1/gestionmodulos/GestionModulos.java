/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.gestionmodulos;

import java.time.DayOfWeek;
import java.time.LocalTime;
import org.daw1.gestionmodulos.entidades.Modulo;
import org.daw1.gestionmodulos.entidades.Sesion;
/**
 *
 * @author rgcenteno
 */
public class GestionModulos {

    private static java.util.Scanner teclado;
    private static java.util.Map<String, Modulo> modulos;
    
    public static void main(String[] args) {
        teclado = new java.util.Scanner(System.in);
        modulos = new java.util.TreeMap<>();
        String input = "";
        do{
            System.out.println("******************************************");
            System.out.println("* 1. Crear módulo                        *");
            System.out.println("* 2. Mostrar horario módulo              *");
            System.out.println("* 3. Editar horario módulo               *");
            System.out.println("* 4. Mostrar listado módulos             *");
            System.out.println("* 5. cambiar duración de sesión          *");
            System.out.println("*                                        *");
            System.out.println("* 0. Salir                               *");
            System.out.println("******************************************");
            input = teclado.nextLine();
            switch(input){
                case "1":
                    Modulo m = crearModulo();
                    if(modulos.containsKey(m.getCodigoModulo())){
                        System.out.println("El módulo ya existe y no se creará");
                    }
                    else{
                        modulos.put(m.getCodigoModulo(), m);
                    }
                    break;
                case "2":
                    System.out.println(modulos);
                    System.out.println("Por favor, inserte el código del módulo del que quiere ver el horario");
                    String codigo = teclado.nextLine();
                    if(modulos.containsKey(codigo)){
                        m = modulos.get(codigo);
                        System.out.println("Sesiones semanales: " + m.getSesionesSemanales() + "\n" + m.getSesionesToString());
                        
                    }
                    else{
                        System.out.println("No existe el módulo seleccionado");
                    }
                    break;
                case "3":
                    do{
                        System.out.println("Por favor, inserte el código del módulo del que quiere ver el horario");
                        codigo = teclado.nextLine();
                        if(!modulos.containsKey(codigo)){
                            System.out.println("El módulo seleccionado no existe");
                        }
                    }
                    while(!modulos.containsKey(codigo));
                    m = modulos.get(codigo);
                    System.out.println("Horario actual del módulo:\n" + m.getSesionesToString());
                    String opcion = "";
                    do{
                        System.out.println("¿Quiere (i)nsertar, (e)liminar o (c)ancelar?");
                        opcion = teclado.nextLine();
                    }
                    while(!opcion.equalsIgnoreCase("i") && !opcion.equalsIgnoreCase("e") && !opcion.equalsIgnoreCase("c"));
                    //Ya sabemos si quiere agregar o eliminar
                    if(opcion.equalsIgnoreCase("i")){
                        Sesion nueva = crearSesion();
                        if(m.agregarSesion(nueva)){
                            System.out.println("Se añadido la sesión correctamente");
                        }
                        else{
                            System.out.println("La sesión no se ha añadido");
                        }
                    }
                    //Caso de eliminar una sesión
                    else if(opcion.equalsIgnoreCase("e")){
                        java.util.SortedSet<Sesion> sesiones = m.getSesiones();
                        if(sesiones.isEmpty()){
                            System.out.println("No hay sesiones asignadas al módulo");
                        }
                        else{
                            int i = 1;
                            for(Sesion s : sesiones){
                                System.out.println(i + " - "+ s);
                                i++;
                            }
                            int sesionAEliminar = -1;
                            do{
                                if(teclado.hasNextInt()){
                                    sesionAEliminar = teclado.nextInt();
                                    if(sesionAEliminar < 1 || sesionAEliminar > sesiones.size()){
                                        System.out.println("Inserte un número válido");
                                    }
                                }
                                teclado.nextLine();
                            }
                            while(sesionAEliminar < 1 || sesionAEliminar > sesiones.size());
                            //Nos ha dado un número válido
                            if(m.quitarSesion(sesionAEliminar)){
                                System.out.println("Se ha borrado la sesión correctamente.\nEstado actual " + m.getSesionesToString());                            
                            }
                            else{
                                System.out.println("No se ha borrado la sesión seleccionada");
                            }
                        }    
                    }                    
                    break;
                case "4":
                    for(java.util.Map.Entry<String, Modulo> entry : modulos.entrySet()){
                        System.out.println(entry.getKey() + ": " + entry.getValue());
                    }
                    break;
                case "5":
                    int duracionNueva = -1;
                    do{
                        System.out.println("Por favor, inserte la nueva duración de las sesiones");
                        if(teclado.hasNextInt()){
                            duracionNueva = teclado.nextInt();
                            if(duracionNueva <= 0){
                                System.out.println("Inserte una duración mayor que cero.");
                            }
                        }
                        teclado.nextLine();
                    }
                    while(duracionNueva <= 0);
                    if(duracionNueva != Modulo.getDuracionSesionModulo()){
                        Modulo.setDuracionSesionModulo(duracionNueva);
                        for(Modulo modulo : modulos.values()){
                            modulo.borrarHorario();
                        }
                        System.out.println("Operación realizada correctamente.");
                    }
                    else{
                        System.out.println("No se ha realizado ningún cambio");
                    }
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Opción inválida");
                    break;
            }
        }
        while(!input.equals("0"));
    }
    
    public static void programaOrdenar(){        
        int elementos = -1;
        do{
            System.out.println("Por favor inserte cuantas sesiones quiere guardar");
            if(teclado.hasNextInt()){
                elementos = teclado.nextInt();
            }
            teclado.nextLine();
        }
        while(elementos < 1);
        
        Sesion[] sesiones = new Sesion[elementos];
        
        for(int i = 0; i < sesiones.length; i++){
            System.out.println("\nCreación de sesión " + (i + 1));
            sesiones[i] = crearSesion();
        }
        
        //Ordenación
        System.out.println(java.util.Arrays.toString(sesiones));
        
        for (int i = 0; i < sesiones.length; i++) {            
            for (int j = i; j < sesiones.length; j++) {                         
                if(sesiones[i].compareTo(sesiones[j]) > 0){                    
                    Sesion aux = sesiones[i];
                    sesiones[i] = sesiones[j];
                    sesiones[j] = aux;
                }
            }
        }
        System.out.println("\nOrdenado:");
        System.out.println(java.util.Arrays.toString(sesiones));
    }
    
    public static Modulo crearModulo(){        
        String nombreModulo = "";
        do{
            System.out.println("Por favor inserte el nombre del módulo:");
            nombreModulo = teclado.nextLine();
        }
        while(nombreModulo.isBlank());
        String nombreProfesor = "";
        do{
            System.out.println("Por favor, inserte el nombre del profesor");
            nombreProfesor = teclado.nextLine();
        }while(nombreProfesor.isBlank());
        String codigoModulo = "";
        do{
            System.out.println("Por favor inserte el código del módulo:");
            codigoModulo = teclado.nextLine();
        }
        while(codigoModulo.length() != 4);
        return new Modulo(codigoModulo, nombreModulo, nombreProfesor);
    }
    
    public static Sesion crearSesion(){
        DayOfWeek dia = null;
        do{
            System.out.println("Por favor inserte el día de la semana (número entre 1 y 7)");
            if(teclado.hasNextInt()){
                int diaInt = teclado.nextInt();
                if(diaInt >= 1 && diaInt <= 7){
                    dia = DayOfWeek.of(diaInt);
                }
                teclado.nextLine();
            }
        }
        while(dia == null);
        
        //Hora de entrada        
        Sesion resultado = null;
        do{
            int horaEntrada = -1;
            do{
                System.out.println("Inserte la hora de entrada (0...23)");
                if(teclado.hasNextInt()){
                    horaEntrada = teclado.nextInt();
                }
                teclado.nextLine();
            }
            while(horaEntrada < 0 || horaEntrada > 23);

            int minutosEntrada = -1;
            do{
                System.out.println("Inserte los minutos de entrada (0...59)");
                if(teclado.hasNextInt()){
                    minutosEntrada = teclado.nextInt();
                }
                teclado.nextLine();
            }
            while(minutosEntrada < 0 || minutosEntrada > 59);

            //Hora de salida
            int horaSalida = -1;
            do{
                System.out.println("Inserte la hora de salida (0...23)");
                if(teclado.hasNextInt()){
                    horaSalida = teclado.nextInt();
                }
                teclado.nextLine();
            }
            while(horaSalida < 0 || horaSalida > 23);

            int minutosSalida = -1;
            do{
                System.out.println("Inserte los minutos de entrada (0...59)");
                if(teclado.hasNextInt()){
                    minutosSalida = teclado.nextInt();
                }
                teclado.nextLine();
            }
            while(minutosSalida < 0 || minutosSalida > 59);

            LocalTime timeEntrada = LocalTime.of(horaEntrada, minutosEntrada);
            LocalTime timeSalida = LocalTime.of(horaSalida, minutosSalida);
            if(timeSalida.compareTo(timeEntrada) < 0){
                System.out.println("La hora de salida debe ser posterior a la hora de entrada");
            }
            else{    
                try{
                    resultado = new Sesion(timeEntrada, timeSalida, dia);                
                }
                catch(IllegalArgumentException ex){
                    System.out.println(ex.getMessage());
                }
            }
            
        }
        while(resultado == null);
        return resultado;
    }
    
}
