02 - Crea una clase que represente un módulo de un curso. Los datos que queremos guardar son:

    Nombre del módulo (p. ej. Programación)
    Código del módulo (max 4 caracteres por ejemplo: PROG)
    Sesiones semanales
    Profesor titular
    Horario guardado en una estructura que relacione día de la semana con el horario que almacenará en cuenta las horas de comienzo y fin

Las sesiones tendrán una duración establecida para todos los módulos. Por ejemplo: 50 minutos. Este valor se podría cambiar.

Crea un constructor, los métodos getter de todos los atributos y dos métodos que permitan agregar o quitar horario a un módulo (comprobando que el horario es válido es decir, la hora de inicio es anterior a la hora de fin y entre una hora y otra hay un número de minutos múltiplo de la duración de sesión). A mayores, crea un método que devuelva el número de días que hay clase. Sobreescribe toString() para que muestre Código módulo - Nombre módulo. Sobreescribe compareTo para que ordene por defecto los módulos por código, nombre. Sobreescribe equals para que sea igual si coinciden código y nombre diga que es el mismo módulo. Sobreescribe hashcode teniendo en cuenta esto.

Crea un programa que tenga el siguiente menú:

    Crear módulo.
    Mostrar horarios módulo (Primero muestra un listado de módulos y al seleccionar uno, muestra su horario).
    Editar horario módulo (Primero muestra un listado de módulos y al seleccionarlo se pregunta si se quiere agregar o quitar). Para agregar, se pide día de la semana y hora inicio (por ej. 10:50 y hora fin: 11:30). Si el día tenía horario, se reemplaza, si no se crea el día y se agrega el horario. Si se selecciona borrar, se borra el horario seleccionado)
    Mostrar listado de módulos.
    Cambiar duración de sesión. Cambia la duración de sesión y, si ha cambiado, resetea los horarios de todos los módulos.
